<?php 
    // include data 2020
    include "2020/dataPT.php";
    include "2020/dataCV.php";
    include "2020/dataBUT.php";

    // tinggal ubah classnya
    $data = new dataBUT();

    // pakai variabel yang ada
    $data = $data->data();

    // $z untuk file yg telah di download dan $n untuk yang belum
    $z = $n = 0;

    // array menyimpan nib yang belum terdownload
    $dataGagal = [];

    // proses rename tetapi sekalian untuk cek file
    // tidak perlu ada inputan seperti sebelumnya

    for ($i=0; $i < count($data); $i++) { 
    // for ($i=1200; $i < 1500; $i++) { 

        // memecah string data ke-i yg berisi nama dan nib
        $nib = explode("_", $data[$i]);

        // hanya untuk menambah 0 diawal nib karena tidak ada di data
        $nama = $nib[0].'_0'.$nib[1];

        // fungsi untuk rename | folder menyesuaikan
        // $rename = rename('C:\Users\Syadan Asandy Nugrah\Downloads\Documents\NIB_0'.$nib[1].'.pdf', 'C:\Users\Syadan Asandy Nugrah\Downloads\Documents\NIB_'.$nama.'.pdf');

        // fungsi cek data | folder menyesuaikan
        $rename = rename('C:\Users\Syadan Asandy Nugrah\Downloads\Documents\NIB_'.$nama.'.pdf', 'C:\Users\Syadan Asandy Nugrah\Downloads\Documents\NIB_'.$nama.'.pdf');

        if (!$rename) {
            $n+=1;

            //memasukkan nib yang belum terdownload ke dalam array
            array_push($dataGagal, $nib[1]);
        }
        else {
            $z+=1;
        }

        // batas cek data
    }

    echo "<p><b> Total Berhasil di Download: ". $z ."</b><br>";
    echo "<b> Total Gagal di Download: ". $n ."</b>";
    echo "<p> NIB : <p>";

    //menampilkan nib yang tidak terdownload
    for ($i=0; $i < count($dataGagal); $i++) { 
        echo "'".$dataGagal[$i]."',<br>";
    }
  
?>