// input nib disini
// tidak ada batasan banyaknya data
// hanya saja sistem di set untuk dapat mendownload data dengan rata-rata sebanyak 15 per menit

var nib = [
                                                                      
];

// batas untuk menghentikan interval
let n = 0;

// keseluruhan fungsi didalam akan di eksekusi setiap 4000 milisecond atau 4 detik
var z = setInterval(() => {
    $(document).ready(function(){

        // set nilai pada search form berdasarkan nib
        // semua set nilai dan klik otomatis akan melihat atau inspect elemen pada web terlebih dahulu
        // guna mengambil id atau class masing elemen
        document.getElementById("tb_cariflist_perusahaan").value = nib[n];

        // klik secara otomatis btn search
        $('#btn_cariflist_perusahaan').trigger('click');

        // increment var n
        n+=1;

        // sebelum code di bawah di eksekusi akan ditunggu selama 2 detik
        // guna mencegah terjadinya collision
        // sebab browser akan melakukan pencarian data hingga siap
        // dan itu terjadi -+ selama 2 detik
        // tergantung kualitas jaringan
        setTimeout(function(){

            // klik otomatis row yang muncul
            $('.rowTable1').trigger('click');

            // klik otomatis menu nib
            $('#tb_menuflist_perusahaan2').trigger('click');

            //klik otomatis btn format pencarian
            $('.btn-danger').trigger('click');

        }, 2000);

        // mencegah code di eksekusi selama 0.25 detik
        setTimeout(() => {

            // klik otomatis modal dialog download data nib
            $('.closejAlert').trigger('click');

        }, 250);
    });

    // pengecekan apakah data yang di masukkan ke dalam variabel nib sudah di eksekusi semua atau belum
    if(n == nib.length-1) {

        // menghentikan interval atau proses
        clearInterval(z);
    }

}, 4000);